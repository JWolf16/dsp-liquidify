﻿using System;
using BepInEx;
using HarmonyLib;
using Liquidify.data;
using System.Reflection;
using System.IO;
using BepInEx.Logging;
using Newtonsoft.Json;

namespace Liquidify
{
    [BepInPlugin("ca.jwolf.plugin.DSP.Liquidify", "Liquidify", "1.0.0")]
    public class LiqCore: BaseUnityPlugin
    {
        private Harmony harmony;
        internal static Settings settings = new Settings();
        internal static ManualLogSource mLogger;

        internal void Awake()
        {
            mLogger = Logger;
            harmony = new Harmony("ca.jwolf.plugin.DSP.Liquidify");
            try
            {
                harmony.PatchAll(Assembly.GetExecutingAssembly());
            }
            catch (Exception e)
            {
                Logger.LogError(e);
            }

            try
            {
                string path = Info.Location;
                using (StreamReader reader = new StreamReader($"{Path.GetDirectoryName(path)}/settings.json"))
                {
                    string jdata = reader.ReadToEnd();
                    settings = JsonConvert.DeserializeObject<Settings>(jdata);
                    settings.buildMap();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e);
            }
        }

        internal void OnDestroy()
        {
            harmony.UnpatchSelf();
        }
    }
}
