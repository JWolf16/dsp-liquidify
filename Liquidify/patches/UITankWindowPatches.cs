using System;
using Liquidify;
using Liquidify.data;
using HarmonyLib;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Liquidify.patches
{
    [HarmonyPatch(typeof(UITankWindow), "_OnUpdate")]
    class UITankWindow_OnUpdate_Patch
    {
        static bool Prefix(UITankWindow __instance)
        {
            if (__instance.tankId == 0 || __instance.factory == null)
            {
              __instance._Close();
            }
            else
            {
              TankComponent tankComponent1 = __instance.storage.tankPool[__instance.tankId];
              if (tankComponent1.id != __instance.tankId)
              {
                __instance._Close();
              }
              else
              {
                if (__instance.pointerPress)
                {
                  if (__instance.pointerPut)
                  {
                    if (__instance.player.inhandItemId > 0 && ItemProto.isFluid(__instance.player.inhandItemId) && __instance.player.inhandItemCount > 0 && (__instance.player.inhandItemId == tankComponent1.fluidId || tankComponent1.fluidId == 0 || tankComponent1.fluidCount == 0))
                    {
                      int cnt = __instance.player.inhandItemCount > 1 ? 2 : 1;
                      int num = tankComponent1.fluidCapacity - tankComponent1.fluidCount;
                      if (num < cnt)
                        cnt = num;
                      if (cnt > 0)
                      {
                        __instance.player.UseHandItems(cnt);
                        if (__instance.storage.tankPool[__instance.tankId].fluidId == 0)
                          __instance.storage.tankPool[__instance.tankId].fluidId = __instance.player.inhandItemId;
                        __instance.storage.tankPool[__instance.tankId].fluidCount += cnt;
                        if (__instance.player.inhandItemCount <= 0)
                          __instance.player.SetHandItems(0, 0);
                      }
                    }
                  }
                  else if ((__instance.player.inhandItemId == 0 || __instance.player.inhandItemId == tankComponent1.fluidId) && (tankComponent1.fluidId > 0 && tankComponent1.fluidCount > 0))
                  {
                    int addcnt = tankComponent1.fluidCount > 1 ? 2 : 1;
                    __instance.player.SetHandItemId_Unsafe(tankComponent1.fluidId);
                    __instance.player.AddHandItemCount_Unsafe(addcnt);
                    __instance.storage.tankPool[__instance.tankId].fluidCount -= addcnt;
                    if (__instance.storage.tankPool[__instance.tankId].fluidCount == 0)
                      __instance.storage.tankPool[__instance.tankId].fluidId = 0;
                  }
                }
                TankComponent tankComponent2 = __instance.storage.tankPool[__instance.tankId];
                if (__instance.pointerIn || __instance.pointerPress)
                {
                  if (__instance.player.inhandItemId > 0 && ItemProto.isFluid(__instance.player.inhandItemId) && __instance.player.inhandItemCount > 0 && (__instance.player.inhandItemId == tankComponent2.fluidId || tankComponent2.fluidId == 0 || tankComponent2.fluidCount == 0) && (!__instance.pointerPress && !VFInput.shift && !VFInput.control || __instance.pointerPress && __instance.pointerPut))
                  {
                    __instance.actionImage0.gameObject.SetActive(true);
                    __instance.actionImage0.sprite = __instance.injectSprite;
                    __instance.actionText.text = "注入流体".Translate();
                  }
                  else if ((__instance.player.inhandItemId == 0 || __instance.player.inhandItemId == tankComponent2.fluidId) && (tankComponent2.fluidId > 0 && tankComponent2.fluidCount > 0) && (!__instance.pointerPress || __instance.pointerPress && !__instance.pointerPut))
                  {
                    __instance.actionImage0.gameObject.SetActive(true);
                    __instance.actionImage0.sprite = __instance.extractSprite;
                    __instance.actionText.text = "抽取流体".Translate();
                  }
                  else
                  {
                    __instance.actionImage0.gameObject.SetActive(false);
                    __instance.actionText.text = "";
                  }
                }
                else
                  __instance.actionImage0.gameObject.SetActive(false);
                if (tankComponent2.fluidId > 0)
                {
                  ItemProto itemProto = LDB.items.Select(tankComponent2.fluidId);
                  ItemDef itemDef;
                  if (LiqCore.settings.itemMap.TryGetValue(itemProto.ID, out itemDef))
                  {
                    __instance.exchangeAndColoring(itemDef.GetColor());
                  }
                  else
                  {
                    if (itemProto.ID == 1000)
                      __instance.exchangeAndColoring(__instance.waterColor);
                    else if (itemProto.ID == 1114)
                      __instance.exchangeAndColoring(__instance.refinedOilColor);
                    else if (itemProto.ID == 1007)
                      __instance.exchangeAndColoring(__instance.oilColor);
                    else if (itemProto.ID == 1116)
                      __instance.exchangeAndColoring(__instance.sulfuricAcidColor);
                    else if (itemProto.ID == 1120)
                      __instance.exchangeAndColoring(__instance.hydrogenColor);
                    else if (itemProto.ID == 1121)
                    {
                      __instance.exchangeAndColoring(__instance.heavyHydrogenColor);
                    }
                    else
                    {
                      Color red = Color.red;
                      red.a *= 0.5f;
                      __instance.countLabel.color = red;
                      __instance.countText.color = red;
                      __instance.circle0.color = red;
                      __instance.circle1.color = red;
                      __instance.circle2.color = red;
                      __instance.circle3.color = red;
                    }
                  }
                  __instance.productIcon.enabled = true;
                  __instance.productIcon.sprite = itemProto.iconSprite;
                }
                else
                {
                  __instance.exchangeAndColoring(__instance.oriColor);
                  __instance.productIcon.enabled = false;
                  if (__instance.pointerIn)
                  {
                    ItemDef itemDef;
                    if (LiqCore.settings.itemMap.TryGetValue(__instance.player.inhandItemId, out itemDef))
                    {
                      __instance.exchangeAndColoringAction(itemDef.GetColor());
                    }
                    else
                    {
                      if (__instance.player.inhandItemId == 1000)
                        __instance.exchangeAndColoringAction(__instance.waterColor);
                      else if (__instance.player.inhandItemId == 1114)
                        __instance.exchangeAndColoringAction(__instance.refinedOilColor);
                      else if (__instance.player.inhandItemId == 1007)
                        __instance.exchangeAndColoringAction(__instance.oilColor);
                      else if (__instance.player.inhandItemId == 1116)
                        __instance.exchangeAndColoringAction(__instance.sulfuricAcidColor);
                      else if (__instance.player.inhandItemId == 1120)
                        __instance.exchangeAndColoringAction(__instance.hydrogenColor);
                      else if (__instance.player.inhandItemId == 1121)
                        __instance.exchangeAndColoringAction(__instance.heavyHydrogenColor);
                      else
                        __instance.exchangeAndColoringAction(__instance.oriColor);
                    }
                  }
                }
                __instance.productTipButton.tips.itemId = tankComponent2.fluidId;
                __instance.countText.text = tankComponent2.fluidCount.ToString();
                float num1 = (float) tankComponent2.fluidCount / (float) tankComponent2.fluidCapacity;
                __instance.circle1.fillAmount = num1;
                Vector2 anchoredPosition1 = __instance.circle2RT.anchoredPosition;
                Vector2 anchoredPosition2 = __instance.circle3RT.anchoredPosition;
                float height1 = __instance.circle2RT.rect.height;
                float y1 = num1 * height1 - height1;
                float x1 = anchoredPosition1.x - 0.65f;
                float x2 = anchoredPosition2.x + 0.45f;
                Rect rect;
                if ((double) tankComponent2.fluidCount <= (double) tankComponent2.fluidCapacity * 0.5)
                {
                  float y2 = num1 + 0.5f;
                  __instance.circle2RT.localScale = new Vector3(1f, y2, 1f);
                  __instance.circle3RT.localScale = new Vector3(1f, y2, 1f);
                  if (tankComponent2.fluidCount > 0)
                  {
                    double num2 = (double) y1;
                    double num3 = 1.0 - (double) y2;
                    rect = __instance.circle2RT.rect;
                    double height2 = (double) rect.height;
                    double num4 = num3 * height2 * 0.5;
                    y1 = (float) (num2 + num4);
                  }
                }
                else
                {
                  __instance.circle2RT.localScale = Vector3.one;
                  __instance.circle3RT.localScale = Vector3.one;
                }
                if (tankComponent2.fluidCount >= tankComponent2.fluidCapacity || tankComponent2.fluidCount <= 0)
                {
                  x1 = 0.0f;
                  x2 = 0.0f;
                  if (tankComponent2.fluidCount > 0)
                  {
                    __instance.circle2RT.localScale = new Vector3(2f, 2f, 2f);
                    __instance.circle3RT.localScale = new Vector3(2f, 2f, 2f);
                  }
                  else
                  {
                    __instance.circle2RT.localScale = Vector3.one;
                    __instance.circle3RT.localScale = Vector3.one;
                  }
                }
                else
                {
                  double num2 = (double) x1;
                  rect = __instance.circle2RT.rect;
                  double num3 = -(double) rect.width / 4.0;
                  if (num2 <= num3)
                  {
                    rect = __instance.circle2RT.rect;
                    x1 = rect.width / 4f;
                  }
                  double num4 = (double) x2;
                  rect = __instance.circle3RT.rect;
                  double num5 = (double) rect.width / 4.0;
                  if (num4 >= num5)
                  {
                    rect = __instance.circle3RT.rect;
                    x2 = (float) (-(double) rect.width / 4.0);
                  }
                }
                __instance.circle2RT.anchoredPosition = new Vector2(x1, y1);
                __instance.circle3RT.anchoredPosition = new Vector2(x2, y1);
                if (tankComponent2.inputSwitch)
                {
                  __instance.inputSwitchText.text = "储液罐开".Translate();
                  __instance.inputButton.transitions[0].mouseoverColor = __instance.openMouseOverColor;
                  __instance.inputButton.transitions[0].pressedColor = __instance.openPressColor;
                  __instance.inputButton.transitions[0].normalColor = __instance.openNormalColor;
                }
                else
                {
                  __instance.inputSwitchText.text = "储液罐关".Translate();
                  __instance.inputButton.transitions[0].mouseoverColor = __instance.closeMouseOverColor;
                  __instance.inputButton.transitions[0].pressedColor = __instance.closePressColor;
                  __instance.inputButton.transitions[0].normalColor = __instance.closeNormalColor;
                }
                if (tankComponent2.outputSwitch)
                {
                  __instance.outputSwitchText.text = "储液罐开".Translate();
                  __instance.outputButton.transitions[0].mouseoverColor = __instance.openMouseOverColor;
                  __instance.outputButton.transitions[0].pressedColor = __instance.openPressColor;
                  __instance.outputButton.transitions[0].normalColor = __instance.openNormalColor;
                }
                else
                {
                  __instance.outputSwitchText.text = "储液罐关".Translate();
                  __instance.outputButton.transitions[0].mouseoverColor = __instance.closeMouseOverColor;
                  __instance.outputButton.transitions[0].pressedColor = __instance.closePressColor;
                  __instance.outputButton.transitions[0].normalColor = __instance.closeNormalColor;
                }
                __instance.inputButton.updating = true;
                __instance.outputButton.updating = true;
              }
            }

            return false;
        }
    }
}