using Liquidify;
using Liquidify.data;
using HarmonyLib;
using System.Collections.Generic;

namespace Liquidify.patches
{
    [HarmonyPatch(typeof(ItemProto), "InitFluids")]
    class ItemProto_InitFluids_Patch
    {
        static void Postfix(ItemProto __instance)
        {
            List<int> intList = new List<int>();
            for (int index = 0; index < ItemProto.fluids.Length; ++index)
                intList.Add(ItemProto.fluids[index]);
            foreach (ItemDef item in LiqCore.settings.items)
            {
              intList.Add(item.itemId);  
            }
            ItemProto.fluids = intList.ToArray();
        }
    }
}