using Liquidify.Data;
using Newtonsoft.Json;
using UnityEngine;

namespace Liquidify.data
{
    public class ItemDef
    {
        public int itemId = 0;
        public string liquidColour = "#FFFFFF";
        
        [JsonIgnore]
        private Color _color;

        private bool cSet = false;

        public Color GetColor()
        {
            if (!cSet)
            {
                ColorUtility.TryParseHtmlString(liquidColour, out _color);
                cSet = true;
            }
            return _color;
        }
    }
}