using System.Collections.Generic;
using Newtonsoft.Json;

namespace Liquidify.data
{
    public class Settings
    {
        public List<ItemDef> items = new List<ItemDef>();
        [JsonIgnore] public Dictionary<int, ItemDef> itemMap = new Dictionary<int, ItemDef>();

        public void buildMap()
        {
            itemMap = new Dictionary<int, ItemDef>();
            foreach (ItemDef item in items)
            {
                itemMap[item.itemId] = item;
            }
        }
    }
}